var data
var categories = []
var values = []
fetch("https://raw.githubusercontent.com/Natural-Healing/Database/master/Products.json")
    .then((resp) => {
        return resp.json()
    })
    .then((i) => {
        data = i
        start()
    })

function start() {
    console.log(data)
    data.forEach((i) => {
        var i2 = Object.values(i)
        values.push(i2)
        if (i2[0].toString() == " " || i2[0].toString() == "") {
            if (!categories.includes("Others")) {
                categories.push("Others")
            }
        } else {
            if (!categories.includes(i2[0])) {
                categories.push(i2[0].toString())
            }
        }
    })

    categories.forEach((i) => {
        var element = document.createElement("section")
        element.classList.add("cards")
        element.value = i.toString()
        document.getElementById("main_container").appendChild(element)
        Array.from(document.getElementsByClassName("cards")).forEach((i2) => {
            if (i2.value == i.toString()) {
                var element1 = document.createElement("div")
                element1.innerHTML = "&nbsp;&nbsp;" + i.toString()
                element1.classList.add("category")
                i2.appendChild(element1)
            }
        })
    })

    values.forEach((i) => {
        Array.from(document.getElementsByClassName("cards")).forEach((i2) => {
            var category = i[0].toString()
            if (category == " " || category == "") {
                category = "Others"
            }
            if (i2.value == category) {
                var element1 = document.createElement("div")
                element1.classList.add("card")
                if (!i[5] == "") {
                    element1.classList.add("pointer")
                    element1.addEventListener("click", () => {
                        window.open(i[5].toString())
                    })
                }
                element1.innerHTML = `
                <div class="card__image-container"><img src="${i[4]}" alt="${i[1].toString()}" /></div><div class="card__content"><p class="card__title text--medium">${i[1].toString()}</p><div class="card__info"><p class="text--medium">${i[2].toString()}</p><p class="card__price text--medium">RM ${parseFloat(i[3]).toFixed(2)}</p></div></div>`
                i2.appendChild(element1)
            }
        })
    })
}
